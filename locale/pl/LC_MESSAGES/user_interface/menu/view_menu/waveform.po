# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-02-27 00:38+0000\n"
"PO-Revision-Date: 2022-02-18 19:00+0100\n"
"Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>\n"
"Language-Team: Polish <kde-i18n-doc@kde.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 22.03.70\n"

#: ../../user_interface/menu/view_menu/waveform.rst:13
msgid "Waveform"
msgstr "Kształt fali"

#: ../../user_interface/menu/view_menu/waveform.rst:16
msgid "Contents"
msgstr "Treść"

#: ../../user_interface/menu/view_menu/waveform.rst:18
msgid ""
"This data is a 3D histogram.  It represents the Luma component (whiteness) "
"of the video. It is the same type of graph as for the :ref:`rgb_parade`. The "
"horizontal axis represents the horizontal axis in the video frame. The "
"vertical axis is the pixel luma from 0 to 255. The brightness of the point "
"on the graph represents the count of the number of pixels with this luma in "
"this column of pixels in the video frame."
msgstr ""
"Te dane to histogram 3D. Przestawia składnik Lumy (białość) nagrania. Jest "
"to ten sam rodzaj wykresu co dla :ref:`rgb_parade`. Oś pozioma przedstawia "
"oś poziomą na klatce nagrania. Oś pionowa jest lumą piksela od 0 do 255. "
"Jasność punktu na wykresie przestawia liczbę pikseli o tej lumie w tej "
"kolumnie pikseli na klatce nagrania."

#: ../../user_interface/menu/view_menu/waveform.rst:26
#, fuzzy
#| msgid ""
#| "For more information see `Granjow's blog <http://kdenlive.org/users/"
#| "granjow/introducing-color-scopes-waveform-and-rgb-parade>`_ on the "
#| "waveform and RGB parade scopes. This blog gives some information on how "
#| "to use the data provided by the RGB parade to do color correction on "
#| "video footage."
msgid ""
"For more information see :ref:`Granjow's blog <waveform_and_RGB_parade>` on "
"the waveform and RGB parade scopes. This blog gives some information on how "
"to use the data provided by the RGB parade to do color correction on video "
"footage."
msgstr ""
"Po więcej szczegółów, zobacz `Blog Granjowa <http://kdenlive.org/users/"
"granjow/introducing-color-scopes-waveform-and-rgb-parade>`_ na temat "
"kształtu fali oraz zakresów parady RGB. Ten blog uczy trochę o wykorzystaniu "
"danych dostarczanych przez paradę RGB do poprawiania barw na nagraniach."

#: ../../user_interface/menu/view_menu/waveform.rst:30
msgid ":ref:`scopes_directx`"
msgstr ":ref:`scopes_directx`"
