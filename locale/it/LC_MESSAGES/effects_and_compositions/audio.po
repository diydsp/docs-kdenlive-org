# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Paolo Zamponi <zapaolo@email.it>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-29 00:22+0000\n"
"PO-Revision-Date: 2022-03-05 14:35+0100\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.2\n"

#: ../../effects_and_compositions/audio.rst:11
msgid "Audio"
msgstr "Audio"

#: ../../effects_and_compositions/audio.rst:13
msgid ""
"Kdenlive has some tools for handling audio. Beside the audio spectrum viewer "
"and some audio effects, you have following possibilities:"
msgstr ""
"Kdenlive ha alcuni strumenti per maneggiare l'audio. Accanto al "
"visualizzatore di spettro e ad alcuni effetti audio, hai le seguenti "
"possibilità:"

#: ../../effects_and_compositions/audio.rst:18
msgid "Audio Mixer"
msgstr "Mixer audio"

#: ../../effects_and_compositions/audio.rst:25
msgid "The audio mixer has following function for each channel:"
msgstr "Il mixer audio ha le seguenti funzioni per ciascun canale:"

#: ../../effects_and_compositions/audio.rst:27
msgid "Channel number (audio track number) or Master channel"
msgstr "Numero del canale (numero della traccia audio), o canale Principale"

#: ../../effects_and_compositions/audio.rst:28
msgid "Mute an audio channel"
msgstr "Silenzia un canale audio"

#: ../../effects_and_compositions/audio.rst:29
msgid "Solo an audio channel"
msgstr "Riproduce solamente un canale audio"

#: ../../effects_and_compositions/audio.rst:30
msgid "Record audio direct on the track of the related audio channel"
msgstr "Registra l'audio direttamente nella traccia del canale audio relativo"

#: ../../effects_and_compositions/audio.rst:31
msgid "Opens the effect stack of the related audio channel"
msgstr "Apre la pila degli effetti del canale audio relativo"

#: ../../effects_and_compositions/audio.rst:32
msgid "Balance the audio channel. Either with the slider or with values"
msgstr "Bilancia i canali audio, sia col cursore che mediante i valori"

#: ../../effects_and_compositions/audio.rst:33
msgid "Adjustment of the volume"
msgstr "Regolazione del volume"

#: ../../effects_and_compositions/audio.rst:36
msgid "Multiple audio streams"
msgstr "Flussi audio multipli"

#: ../../effects_and_compositions/audio.rst:40
msgid ""
"Multiple audio streams of a video clip. In clip properties on the tab audio "
"you can adjust and manipulate each audio stream. More details see here :ref:"
"`audio_properties`"
msgstr ""
"Più flussi audio di una clip video. Nelle scheda Audio delle proprietà delle "
"clip puoi regolare e manipolare ciascun flusso audio. Per maggiori dettagli, "
"vedi :ref:`audio_properties`"
