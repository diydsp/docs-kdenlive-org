# Translation of docs_kdenlive_org_user_interface___shortcuts.po to Catalan
# Copyright (C) 2021-2022 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-16 00:37+0000\n"
"PO-Revision-Date: 2022-01-16 10:57+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../user_interface/shortcuts.rst:1
msgid "Set your own shortcuts in Kdenlive video editor"
msgstr "Configureu dreceres pròpies a l'editor de vídeo Kdenlive"

#: ../../user_interface/shortcuts.rst:1
msgid ""
"KDE, Kdenlive, shortcuts, set, documentation, user manual, video editor, "
"open source, free, learn, easy"
msgstr ""
"KDE, Kdenlive, dreceres, configuració, documentació, manual d'usuari, editor "
"de vídeo, codi lliure, lliure, aprendre, fàcil"

#: ../../user_interface/shortcuts.rst:20
msgid "Keyboard Shortcuts"
msgstr "Dreceres de teclat"

#: ../../user_interface/shortcuts.rst:24
msgid "Contents"
msgstr "Contingut"

#: ../../user_interface/shortcuts.rst:32
msgid "Editing"
msgstr "Edició"

#: ../../user_interface/shortcuts.rst:39
msgid "Action"
msgstr "Acció"

#: ../../user_interface/shortcuts.rst:40
msgid "Shortcut"
msgstr "Drecera"

#: ../../user_interface/shortcuts.rst:41
msgid "Make a cut (make sure track is selected first)"
msgstr "Crea un retall (cal assegurar que abans s'ha seleccionat una pista)"

#: ../../user_interface/shortcuts.rst:42
msgid ":kbd:`Shift + R`"
msgstr ":kbd:`Maj + R`"

#: ../../user_interface/shortcuts.rst:43
msgid "Play"
msgstr "Reprodueix"

#: ../../user_interface/shortcuts.rst:44
msgid ":kbd:`Space`"
msgstr ":kbd:`Espai`"

#: ../../user_interface/shortcuts.rst:45
msgid "Play zone"
msgstr "Reprodueix la zona"

#: ../../user_interface/shortcuts.rst:46
msgid ":kbd:`Ctrl + Space`"
msgstr ":kbd:`Ctrl + Espai`"

#: ../../user_interface/shortcuts.rst:47
msgid "Render"
msgstr "Renderitza"

#: ../../user_interface/shortcuts.rst:48
msgid ":kbd:`Ctrl + Return`"
msgstr ":kbd:`Ctrl + Retorn`"

#: ../../user_interface/shortcuts.rst:49
msgid "Switch Monitor"
msgstr "Commuta el monitor"

#: ../../user_interface/shortcuts.rst:50
msgid ":kbd:`T`"
msgstr ":kbd:`T`"

#: ../../user_interface/shortcuts.rst:51
msgid "Forward"
msgstr "Endavant"

#: ../../user_interface/shortcuts.rst:52
msgid ":kbd:`L`"
msgstr ":kbd:`L`"

#: ../../user_interface/shortcuts.rst:53
msgid "Rewind"
msgstr "Rebobina"

#: ../../user_interface/shortcuts.rst:54
msgid ":kbd:`J`"
msgstr ":kbd:`J`"

#: ../../user_interface/shortcuts.rst:55
msgid "Forward 1 frame"
msgstr "Avança 1 fotograma"

#: ../../user_interface/shortcuts.rst:56
msgid ":kbd:`Right`"
msgstr ":kbd:`Dreta`"

#: ../../user_interface/shortcuts.rst:57
msgid "Rewind 1 frame"
msgstr "Rebobina 1 fotograma"

#: ../../user_interface/shortcuts.rst:58
msgid ":kbd:`Left`"
msgstr ":kbd:`Esquerra`"

#: ../../user_interface/shortcuts.rst:59
msgid "Forward 1 second"
msgstr "Avança 1 segon"

#: ../../user_interface/shortcuts.rst:60
msgid ":kbd:`Shift + Right`"
msgstr ":kbd:`Maj + Dreta`"

#: ../../user_interface/shortcuts.rst:61
msgid "Rewind 1 second"
msgstr "Rebobina 1 segon"

#: ../../user_interface/shortcuts.rst:62
msgid ":kbd:`Shift + Left`"
msgstr ":kbd:`Maj + Esquerra`"

#: ../../user_interface/shortcuts.rst:63
msgid "Toggle Full Screen Mode on and off"
msgstr "Alterna el mode de pantalla completa"

#: ../../user_interface/shortcuts.rst:64
msgid ":kbd:`Ctrl + Shift + F`"
msgstr ":kbd:`Ctrl + Maj + F`"

#: ../../user_interface/shortcuts.rst:65
msgid "Go to Clip End"
msgstr "Ves al final del clip"

#: ../../user_interface/shortcuts.rst:66
msgid ":kbd:`End`"
msgstr ":kbd:`Fi`"

#: ../../user_interface/shortcuts.rst:67
msgid "Go to Clip Start"
msgstr "Ves a l'inici del clip"

#: ../../user_interface/shortcuts.rst:68
msgid ":kbd:`Home`"
msgstr ":kbd:`Inici`"

#: ../../user_interface/shortcuts.rst:69
msgid "Go to Next Snap Point"
msgstr "Ves al punt d'ajust següent"

#: ../../user_interface/shortcuts.rst:70
msgid ":kbd:`Alt + Right`"
msgstr ":kbd:`Alt + Dreta`"

#: ../../user_interface/shortcuts.rst:71
msgid "Go to Previous Snap Point"
msgstr "Ves al punt d'ajust anterior"

#: ../../user_interface/shortcuts.rst:72
msgid ":kbd:`Alt + Left`"
msgstr ":kbd:`Alt + Esquerra`"

#: ../../user_interface/shortcuts.rst:73
msgid "Go to Project End"
msgstr "Ves al final del projecte"

#: ../../user_interface/shortcuts.rst:74
msgid ":kbd:`Ctrl + End`"
msgstr ":kbd:`Ctrl + Fi`"

#: ../../user_interface/shortcuts.rst:75
msgid "Go to Project Start"
msgstr "Ves a l'inici del projecte"

#: ../../user_interface/shortcuts.rst:76
msgid ":kbd:`Ctrl + Home`"
msgstr ":kbd:`Ctrl + Inici`"

#: ../../user_interface/shortcuts.rst:77
msgid "Go to Zone End"
msgstr "Ves al final de la zona"

#: ../../user_interface/shortcuts.rst:78
msgid ":kbd:`Shift + O`"
msgstr ":kbd:`Maj + O`"

#: ../../user_interface/shortcuts.rst:79
msgid "Go to Zone Start"
msgstr "Ves a l'inici de la zona"

#: ../../user_interface/shortcuts.rst:80
msgid ":kbd:`Shift + I`"
msgstr ":kbd:`Maj + I`"

#: ../../user_interface/shortcuts.rst:81
msgid "Group Clips"
msgstr "Agrupa els clips"

#: ../../user_interface/shortcuts.rst:82
msgid ":kbd:`Ctrl + G`"
msgstr ":kbd:`Ctrl + G`"

#: ../../user_interface/shortcuts.rst:83
msgid "Set Zone In"
msgstr "Estableix l'inici de la zona"

#: ../../user_interface/shortcuts.rst:84
msgid ":kbd:`I`"
msgstr ":kbd:`I`"

#: ../../user_interface/shortcuts.rst:85
msgid "Ungroup Clips"
msgstr "Desagrupa els clips"

#: ../../user_interface/shortcuts.rst:86
msgid ":kbd:`Ctrl + Shift + G`"
msgstr ":kbd:`Ctrl + Maj + G`"

#: ../../user_interface/shortcuts.rst:87
msgid "Set Zone Out"
msgstr "Estableix el final de la zona"

#: ../../user_interface/shortcuts.rst:88
msgid ":kbd:`O`"
msgstr ":kbd:`O`"

#: ../../user_interface/shortcuts.rst:89
msgid ":ref:`editing`"
msgstr ":ref:`editing`"

#: ../../user_interface/shortcuts.rst:90
msgid ":kbd:`Ctrl + X` [1]_"
msgstr ":kbd:`Ctrl + X` [1]_"

#: ../../user_interface/shortcuts.rst:93
msgid "available in bleeding edge version > 0.9.10 (Jan2015)"
msgstr "disponible a la versió d'última generació > 0.9.10 (Gen2015)"
