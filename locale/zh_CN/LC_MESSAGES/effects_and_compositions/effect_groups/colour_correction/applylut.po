msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-11 00:38+0000\n"
"PO-Revision-Date: 2022-07-02 11:00\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_effects_and_compositions___effect_groups___colour_correction___applylut."
"pot\n"
"X-Crowdin-File-ID: 25879\n"

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:11
msgid "Apply LUT"
msgstr "Apply LUT - 应用 LUT"

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:13
msgid "Contents"
msgstr "目录"

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:15
msgid ""
"This is the `Avfilter lut3d <https://www.mltframework.org/plugins/"
"FilterAvfilter-lut3d/>`_ MLT filter."
msgstr ""
"这是 `Avfilter lut3d <https://www.mltframework.org/plugins/FilterAvfilter-"
"lut3d/>`_ MLT 滤镜。"

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:17
msgid ""
"Apply a 3D Look Up Table (LUT) to the video. A LUT is an easy way to correct "
"the color of a video."
msgstr ""
"对视频应用 3D 查看表 (Look Up Table, LUT)。LUT 是一个简单的用于校正视频颜色的"
"方法。"

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:19
msgid "**Supported formats:**"
msgstr "**支持的格式：**"

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:21
msgid ".3dl (AfterEffects), .cube (Iridas), .dat (DaVinci), .m3d (Pandora)"
msgstr ""
".3dl (Adobe AfterEffects), .cube (Iridas), .dat (DaVinci), .m3d (Pandora)"

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:23
msgid "**Parameters:**"
msgstr "**参数：**"

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:25
msgid "Filename: File containing the LUT to be applied."
msgstr "文件名：要应用的 LUT 文件。"

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:27
msgid ""
"Interpolation Method: Can be Nearest, Trilinear or Tetrahedral. Defaults to "
"Tetrahedral."
msgstr ""
"插值方法：可以选择 Nearest (最近)、Triilinear (三线式)或 Tetrahedral (四面体"
"式)。默认值为 Tetrahedral。"

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:33
msgid "Example of LUT Filter"
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:35
msgid "Example of Manual workflow Before and after applying LUT"
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:41
msgid "Figure 1 - View of the interface"
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:44
msgid "Simple steps:"
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:46
msgid ""
"FREE LUTS – For our example, we rely on the files that can be downloaded "
"from the address https://goo.gl/OeIFkr"
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:48
msgid ""
"SEVEN CUBE FILES – Download the zip file, just extract it into a folder: "
"each of the files, which is then a simple text file, represents a Look Up "
"Table. Those examples were developed inspired by famous films (whose titles "
"are parodied in the file name)."
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:50
msgid ""
"Once package is downloaded and unpacked, open effects menu in the right "
"corner of the program window."
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:57
msgid "Figure 2 - View of main menu."
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:59
msgid ""
"Then you need to find the LUT effect to apply. There are several, the "
"majority of which are maintained only for backwards compatibility but not "
"for any result. The right one is Apply LUT, in the section Color correction."
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:65
msgid "Figure 3 - Apply LUT"
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:67
msgid ""
"BALANCING – Adjusting color, for example col 3-point balance, you need to "
"insert the effect above that of the LUT. This allows you to correct the "
"image before it reaches the LUT, then obtaining homogeneous results with "
"other clips."
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:73
msgid "Figure 4 - Colour balance"
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:76
msgid "Manually adjust the clip"
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:78
msgid ""
"In addition to the LUT, other effects can be used to manually correct the "
"colors"
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:84
msgid ""
"CHANGE OF SHADES – This allows, for example, to color the background in blue "
"and the actor in orange, widely used in classic Hollywood postproduction "
"setup. A faster but less detailed alternative is Hue shift, which shifts all "
"shades towards red or blue."
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:91
msgid ""
"SATURATION – After changing the coloration of the image, with one of the two "
"effects that is presented, this allows saturate the color differently "
"depending on the brightness by drawing a curve on the canal saturation."
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:97
msgid "Before and After"
msgstr ""
