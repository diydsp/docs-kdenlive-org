msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2022-07-02 11:00\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_effects_and_compositions___effect_groups___misc___luminance."
"pot\n"
"X-Crowdin-File-ID: 26217\n"

#: ../../effects_and_compositions/effect_groups/misc/luminance.rst:14
msgid "Luminance"
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/luminance.rst:16
msgid "Contents"
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/luminance.rst:18
msgid ""
"This is the `Frei0r luminance <https://www.mltframework.org/plugins/"
"FilterFrei0r-luminance/>`_ MLT filter."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/luminance.rst:20
msgid "Creates a luminance map of the image."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/luminance.rst:22
msgid "(Moved to Color section in ver 15.n)."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/luminance.rst:24
msgid "https://youtu.be/2NlTk95kCY8"
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/luminance.rst:26
msgid "https://youtu.be/0wiM77K-ENQ"
msgstr ""
