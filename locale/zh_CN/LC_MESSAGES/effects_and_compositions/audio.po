msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-29 00:22+0000\n"
"PO-Revision-Date: 2022-07-02 11:00\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_effects_and_compositions___audio.pot\n"
"X-Crowdin-File-ID: 26419\n"

#: ../../effects_and_compositions/audio.rst:11
msgid "Audio"
msgstr ""

#: ../../effects_and_compositions/audio.rst:13
msgid ""
"Kdenlive has some tools for handling audio. Beside the audio spectrum viewer "
"and some audio effects, you have following possibilities:"
msgstr ""

#: ../../effects_and_compositions/audio.rst:18
msgid "Audio Mixer"
msgstr ""

#: ../../effects_and_compositions/audio.rst:25
msgid "The audio mixer has following function for each channel:"
msgstr ""

#: ../../effects_and_compositions/audio.rst:27
msgid "Channel number (audio track number) or Master channel"
msgstr ""

#: ../../effects_and_compositions/audio.rst:28
msgid "Mute an audio channel"
msgstr ""

#: ../../effects_and_compositions/audio.rst:29
msgid "Solo an audio channel"
msgstr ""

#: ../../effects_and_compositions/audio.rst:30
msgid "Record audio direct on the track of the related audio channel"
msgstr ""

#: ../../effects_and_compositions/audio.rst:31
msgid "Opens the effect stack of the related audio channel"
msgstr ""

#: ../../effects_and_compositions/audio.rst:32
msgid "Balance the audio channel. Either with the slider or with values"
msgstr ""

#: ../../effects_and_compositions/audio.rst:33
msgid "Adjustment of the volume"
msgstr ""

#: ../../effects_and_compositions/audio.rst:36
msgid "Multiple audio streams"
msgstr ""

#: ../../effects_and_compositions/audio.rst:40
msgid ""
"Multiple audio streams of a video clip. In clip properties on the tab audio "
"you can adjust and manipulate each audio stream. More details see here :ref:"
"`audio_properties`"
msgstr ""
