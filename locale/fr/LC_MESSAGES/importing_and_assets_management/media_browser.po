# Xavier Besnard <xavier.besnard@neuf.fr>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2022-01-03 14:05+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.0\n"

#: ../../importing_and_assets_management/media_browser.rst:11
msgid "Media Browser"
msgstr "Navigateur de média"

#: ../../importing_and_assets_management/media_browser.rst:14
msgid "Contents"
msgstr "Contenu"

#: ../../importing_and_assets_management/media_browser.rst:18
msgid ""
"The new Media Browser allows you to easily navigate through your file system "
"and add clips directly to the Bin or Timeline. You can enable it from View "
"menu."
msgstr ""
"Le nouveau navigateur de média vous permet de naviguer facilement dans votre "
"système de fichiers et d'ajouter des vidéos directement dans le dossier du "
"projet ou dans la frise chronologique. Vous pouvez l'activer à partir du "
"menu « Affichage »."
