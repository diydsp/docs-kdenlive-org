# Xavier Besnard <xavier.besnard@neuf.fr>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-02 00:38+0000\n"
"PO-Revision-Date: 2022-05-16 19:51+0200\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.04.0\n"

#: ../../exporting/render_profile_parameters.rst:1
msgid "The Kdenlive User Manual"
msgstr "Le manuel utilisateur de Kdenlive"

#: ../../exporting/render_profile_parameters.rst:1
msgid ""
"KDE, Kdenlive, documentation, user manual, video editor, open source, free, "
"help, learn, render, render profile, render parameter"
msgstr ""
"KDE, Kdenlive, documentation, manuel utilisateur, éditeur de vidéo, open "
"source, libre, aide, apprendre, rendu, profil de rendu, paramètre de rendu"

#: ../../exporting/render_profile_parameters.rst:21
msgid "Render Profile Parameters"
msgstr "Paramètres du profil de rendu"

#: ../../exporting/render_profile_parameters.rst:24
msgid "Contents"
msgstr "Contenu"

#: ../../exporting/render_profile_parameters.rst:27
#: ../../exporting/render_profile_parameters.rst:92
msgid "Render Profile Parameters - How to read them"
msgstr "Paramètres du profil de rendu - Comment les lire"

#: ../../exporting/render_profile_parameters.rst:44
msgid ""
"Kdenlive now makes use of \"property presets\" delivered by the *melt* "
"project (see `melt doco <https://www.mltframework.org/docs/presets/>`_). "
"These presets are referenced by the *properties=<preset>* syntax. In the "
"example illustrated, the render profile is referencing *lossless/H.264*. "
"This refers to a property preset found in file H.264 found on the system at :"
"file:`/usr/share/mlt/presets/consumer/avformat/lossless`."
msgstr ""
"Kdenlive fait maintenant usage des « pré-réglages de propriétés\" fournis "
"par le projet *melt* (Veuillez consulter la « documentation « melt » "
"<https://www.mltframework.org/docs/presets/> »_). Ces derniers sont "
"référencés par la syntaxe *properties=<preset>*. Dans l'exemple illustré, le "
"profil de rendu fait référence à *sans-perte / H264*. Cela fait référence à "
"un préréglage de propriété trouvé dans le fichier H264 se trouvant sur le "
"système à l'adresse suivante :file:`/usr/share/mlt/presets/consumer/avformat/"
"lossless`."

#: ../../exporting/render_profile_parameters.rst:46
msgid ""
"All the *<presets>* referenced in the render settings in Kdenlive will be "
"referring to presets found at :file:`/usr/share/mlt/presets/consumer/"
"avformat/` (on a default install). Note that you reference presets found in "
"subdirectories of this folder using a :file:`<dirname>/<profile>` syntax as "
"shown in the example above."
msgstr ""
"Tous les *<presets>* référencés dans les paramètres de rendu dans Kdenlive "
"feront référence aux pré-réglages trouvés dans :file:`/usr/share/mlt/presets/"
"consumer/avformat/` (sur une installation par défaut). Veuillez noter que "
"vous faites référence à des préréglages trouvés dans des sous-dossiers de ce "
"dossier en utilisant la syntaxe :file:`<dirname>/<profile>`, comme le montre "
"l'exemple ci-dessus."

#: ../../exporting/render_profile_parameters.rst:55
msgid ""
"The preset files found at :file:`/usr/share/mlt/presets/consumer/avformat/` "
"are simple text files that contain the *melt* parameters that define the "
"rendering. An example is shown below. These are the same parameters that "
"were used in earlier versions of Kdenlive – see next section for how to read "
"those."
msgstr ""
"Les fichiers de préréglages se trouvant dans :file:`/usr/share/mlt/presets/"
"consumer/avformat/` sont de simples fichiers texte contenant les paramètres "
"*melt* définissant le rendu. Un exemple est donné ci-dessous. Ce sont les "
"mêmes paramètres qui étaient utilisés dans les versions précédentes de "
"Kdenlive - Veuillez consulter la section suivante pour savoir comment les "
"lire."

#: ../../exporting/render_profile_parameters.rst:57
msgid "Contents of lossless/H.264:"
msgstr "Contenu de sans-perte / H264 :"

#: ../../exporting/render_profile_parameters.rst:79
msgid "Scanning Dropdown"
msgstr "Liste déroulante de numérisation"

#: ../../exporting/render_profile_parameters.rst:85
msgid ""
"This option controls the frame scanning setting the rendered file will have. "
"Options are *Force Progressive*, *Force Interlaced* and *Auto*."
msgstr ""
"Cette option contrôle le paramètre de numérisation d'image que le fichier de "
"rendu aura. Les options sont *Forcer en progressif*, *Forcer en entrelacé* "
"et *Auto*."

#: ../../exporting/render_profile_parameters.rst:88
msgid ""
":menuselection:`Auto` causes the rendered file to take the scanning settings "
"that are defined in the :ref:`project_settings`. Use the other options to "
"override the setting defined in the project settings."
msgstr ""
":menuselection:`Auto` fait en sorte que le fichier de rendu prenne les "
"paramètres de numérisation étant définis dans le :ref:`project_settings`. "
"Utilisez les autres options pour remplacer les paramètres définis dans la "
"configuration du projet."

#: ../../exporting/render_profile_parameters.rst:96
msgid "|outdated|"
msgstr "|obsolète|"

#: ../../exporting/render_profile_parameters.rst:98
msgid ""
"The parameters that go into a render profile derive from the **ffmpeg** "
"program."
msgstr ""
"Les paramètres allant dans le profil de rendu proviennent du programme "
"**ffmpeg**."

#: ../../exporting/render_profile_parameters.rst:100
msgid ""
"This is a worked example to show how you can understand what these "
"parameters mean using the **ffmpeg** documentation."
msgstr ""
"Il s'agit d'un exemple dédié pour montrer comment vous pouvez comprendre la "
"signification de ces paramètres en utilisant la documentation **ffmpeg**."

#: ../../exporting/render_profile_parameters.rst:102
msgid "In the example above the parameters are:"
msgstr "Dans l'exemple ci-dessus, les paramètres sont :"

#: ../../exporting/render_profile_parameters.rst:124
msgid ""
"Looking up the `ffmpeg help <https://linux.die.net/man/1/ffmpeg>`_ "
"translates these parameters as shown below."
msgstr ""
"La lecture de l'aide de « ffmpeg <https://linux.die.net/man/1/ffmpeg> »_ "
"présente ces paramètres comme indiqué ci-dessous."

#: ../../exporting/render_profile_parameters.rst:126
msgid "Main option is:"
msgstr "L'option principale est :"

#: ../../exporting/render_profile_parameters.rst:132
msgid "Video options are:"
msgstr "Les options pour la vidéo sont :"

#: ../../exporting/render_profile_parameters.rst:144
msgid "Audio options are:"
msgstr "Les options pour l'audio sont :"

#: ../../exporting/render_profile_parameters.rst:152
msgid "The AVCodecContext AVOptions include:"
msgstr "Le « AVCodecContext AVOptions » intègre :"

#: ../../exporting/render_profile_parameters.rst:161
msgid ""
"So all the render profile options are documented here in the **ffmpeg** "
"documentation."
msgstr ""
"Ainsi, toutes les options de profil de rendu sont documentées ici, dans la "
"documentation **ffmpeg**."

#: ../../exporting/render_profile_parameters.rst:163
msgid ""
"See also `MLT doco <https://www.mltframework.org/docs/presets/>`_ on "
"ConsumerAvFormat."
msgstr ""
"Veuillez aussi consulter la «  documentation MLT <https://www.mltframework."
"org/docs/presets/> »_ sur « ConsumerAvFormat »."

#: ../../exporting/render_profile_parameters.rst:165
msgid ""
"See also `HOWTO Produce 4k and 2K videos, YouTube compatible <https://forum."
"kde.org/viewtopic.php?f=272&amp;t=124869#p329129>`_."
msgstr ""
"Veuillez aussi consulter le tutoriel « Comment produire des vidéos 4k et 2K, "
"compatibles YouTube <https://forum.kde.org/viewtopic.php?f=272&amp;"
"t=124869#p329129> »_."
