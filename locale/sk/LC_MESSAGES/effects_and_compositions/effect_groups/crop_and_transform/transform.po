# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Roman Paholik <wizzardsk@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-28 00:22+0000\n"
"PO-Revision-Date: 2021-11-17 11:24+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.08.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/transform.rst:14
msgid "Transform"
msgstr "Transformovať"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/transform.rst:16
msgid "Contents"
msgstr "Obsah"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/transform.rst:18
msgid ""
"This is the `Qtblend <https://www.mltframework.org/plugins/FilterQtblend/>`_ "
"MLT filter."
msgstr ""

#: ../../effects_and_compositions/effect_groups/crop_and_transform/transform.rst:20
msgid "Manipulates Position, scale and opacity."
msgstr ""

#: ../../effects_and_compositions/effect_groups/crop_and_transform/transform.rst:22
msgid ""
"The Composition mode parameter of the effect is documented on the Qt "
"documentation under `QPainter CompositionMode <https://doc.qt.io/qt-5/"
"qpainter.html#CompositionMode-enum>`_."
msgstr ""
