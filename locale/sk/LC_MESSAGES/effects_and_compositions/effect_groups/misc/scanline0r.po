# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Roman Paholik <wizzardsk@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2021-11-17 11:31+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.08.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../effects_and_compositions/effect_groups/misc/scanline0r.rst:13
msgid "scanline0r"
msgstr "scanline0r"

#: ../../effects_and_compositions/effect_groups/misc/scanline0r.rst:16
msgid "Contents"
msgstr "Obsah"

#: ../../effects_and_compositions/effect_groups/misc/scanline0r.rst:18
msgid ""
"This is the `Frei0r scanline0r <https://www.mltframework.org/plugins/"
"FilterFrei0r-scanline0r/>`_ MLT filter."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/scanline0r.rst:20
msgid "Interlaced black lines."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/scanline0r.rst:22
msgid "https://youtu.be/nJ2TE4SdaJM"
msgstr "https://youtu.be/nJ2TE4SdaJM"

#: ../../effects_and_compositions/effect_groups/misc/scanline0r.rst:24
msgid "https://youtu.be/St4P6Ziwmcw"
msgstr "https://youtu.be/St4P6Ziwmcw"
