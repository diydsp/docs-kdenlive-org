# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-22 20:00+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/obscure.rst:14
msgid "Obscure"
msgstr "Dölj"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/obscure.rst:16
msgid "Contents"
msgstr "Innehåll"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/obscure.rst:18
msgid ""
"See the `Obscure <https://www.mltframework.org/plugins/FilterObscure/>`_ MLT "
"filter."
msgstr ""
"Se MLT-filtret `Dölj <https://www.mltframework.org/plugins/FilterObscure/>`_."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/obscure.rst:20
msgid "Hide a region of the clip."
msgstr "Dölj ett område på klippet."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/obscure.rst:22
msgid "https://youtu.be/NL8cBqJc-WU"
msgstr "https://youtu.be/NL8cBqJc-WU"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/obscure.rst:24
msgid "https://youtu.be/oIu9FQwVx0c"
msgstr "https://youtu.be/oIu9FQwVx0c"
