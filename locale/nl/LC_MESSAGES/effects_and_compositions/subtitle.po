# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-02 00:38+0000\n"
"PO-Revision-Date: 2022-05-02 12:17+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.04.0\n"

#: ../../effects_and_compositions/subtitle.rst:23
msgid "Subtitle"
msgstr "Ondertiteling"

#: ../../effects_and_compositions/subtitle.rst:30
msgid ""
"The subtitling tool allows you to add and edit subtitles directly in the "
"timeline on a special subtitle track or by using the new subtitle window. "
"You can also import (**SRT**/**ASS**) and export (**SRT**) subtitles."
msgstr ""
"Het hulpmiddel voor ondertiteling biedt u het toevoegen en bewerken van "
"ondertitels direct in de tijdlijn op een speciale ondertiteltrack of door "
"het nieuwe ondertitelingsvenster te gebruiken. U kunt ook ondertitels "
"(**SRT**/**ASS**) importeren en ondertitels (**SRT**) exporteren."

#: ../../effects_and_compositions/subtitle.rst:32
msgid "There are 3 ways to add subtitle:"
msgstr "Er zijn drie manieren om ondertiteling toe te voegen:"

#: ../../effects_and_compositions/subtitle.rst:34
msgid "**Menu**"
msgstr "**Menu**"

#: ../../effects_and_compositions/subtitle.rst:36
msgid ":menuselection:`Project --> Subtitle --> Add Subtitle`"
msgstr ":menuselection:`Project --> Ondertiteling --> Ondertitel toevoegen`"

#: ../../effects_and_compositions/subtitle.rst:38
msgid "**Keyboard**"
msgstr "**Toetsenbord**"

#: ../../effects_and_compositions/subtitle.rst:40
msgid ":kbd:`Shift + S` adds a subtitle."
msgstr ":kbd:`Shift + S` voegt een ondertitel toe."

#: ../../effects_and_compositions/subtitle.rst:42
msgid "**Icon and Mouse**"
msgstr "**Pictogram en muis**"

#: ../../effects_and_compositions/subtitle.rst:44
msgid ""
"Click :guilabel:`Add Subtitle` icon in the :ref:`timeline toolbar<guides>` "
"to open the subtitle track in the timeline."
msgstr ""
"Klik op pictogram :guilabel:`Ondertitel toevoegen` in :ref:`timeline "
"toolbar` om de ondertiteltrack in de tijdlijn te openen."

#: ../../effects_and_compositions/subtitle.rst:45
msgid "Double-click in the subtitle track to add a subtitle."
msgstr "Dubbelklik op de ondertiteltrack om een ondertitel toe te voegen."

#: ../../effects_and_compositions/subtitle.rst:47
msgid "**Adding and editing text**"
msgstr ""

#: ../../effects_and_compositions/subtitle.rst:49
msgid ""
"Add or editing text either directly into the subtitle clip or in the "
"subtitle window."
msgstr ""

#: ../../effects_and_compositions/subtitle.rst:51
msgid "**Adjust the length of subtitle**"
msgstr ""

#: ../../effects_and_compositions/subtitle.rst:53
msgid ""
"Grab the end of a subtitle with the mouse and lengthen or shorten it as "
"needed. Set subtitle in/out can be achieved with the same shortcut as to set "
"clip in/out (left/right parenthesis shortcut)."
msgstr ""

#: ../../effects_and_compositions/subtitle.rst:56
msgid "**Subtitle window**"
msgstr ""

#: ../../effects_and_compositions/subtitle.rst:61
msgid ""
"The subtitles window allows easier editing and also makes it possible to "
"easily navigate between subtitles with the :guilabel:`Left` and :guilabel:"
"`Right` buttons."
msgstr ""

#: ../../effects_and_compositions/subtitle.rst:62
msgid "With the plus sign button, you can add subtitles."
msgstr ""

#: ../../effects_and_compositions/subtitle.rst:63
msgid ""
"The scissors are mostly here for divide subtitles: let's say your subtitle "
"text is too long and you want to make it 2 different subtitles. Put the "
"cursor in the text widget where you want to cut and click the scissors, it "
"will split the text between 2 different subtitle items. The scissors are "
"only working when the playhead is over the subtitle itself."
msgstr ""

#: ../../effects_and_compositions/subtitle.rst:64
msgid "The tick button adds the text to the subtitle."
msgstr ""

#: ../../effects_and_compositions/subtitle.rst:67
msgid "**Import and export subtitle**"
msgstr ""

#: ../../effects_and_compositions/subtitle.rst:69
msgid ""
"Importing **SRT** and **ASS** subtitle file: :menuselection:`Project --> "
"Subtitles --> Import Subtitle File`"
msgstr ""
"Ondertitelbestand **SRT** en **ASS**: :menuselection:`Project --> "
"ondertitels --> Ondertitelbestand importeren`"

#: ../../effects_and_compositions/subtitle.rst:71
msgid ""
"Exporting **SRT** subtitles only: :menuselection:`Project --> Subtitles --> "
"Export Subtitle File`"
msgstr ""
"Alleen ondertitels **SRT** exporteren: :menuselection:`Project --> "
"Ondertitels --> Ondertitelbestand exporteren`"

#: ../../effects_and_compositions/subtitle.rst:75
msgid ""
"**SRT** supports markup for: bold, italic, underline, text color and line "
"break."
msgstr ""

#: ../../effects_and_compositions/subtitle.rst:77
msgid "``<b>text in boldface</b>``"
msgstr ""

#: ../../effects_and_compositions/subtitle.rst:78
msgid "``<i>text in italics</i>``"
msgstr ""

#: ../../effects_and_compositions/subtitle.rst:79
msgid "``<u>text underlined</u>``"
msgstr ""

#: ../../effects_and_compositions/subtitle.rst:80
msgid ""
"``<font color=\"#00ff00\"> text in green</font>`` you can use the font tag "
"only to change color."
msgstr ""

#: ../../effects_and_compositions/subtitle.rst:81
msgid ""
"And all combined: ``<font color=\"#00ff00\"><b><i><u>All combined</u></i></"
"b></font>``"
msgstr ""

#: ../../effects_and_compositions/subtitle.rst:82
msgid ""
"**Line break:** Add on the end of each line a ``<br>`` (for break). Now the :"
"file:`.srt` file is stored correct and reopened with the line break. The "
"subtitle in the subtitle window will be all in 1 line after several save but "
"the breaks is working."
msgstr ""

#: ../../effects_and_compositions/subtitle.rst:84
msgid ":kbd:`Alt + arrow` jumps from subtitle to subtitle."
msgstr ""

#: ../../effects_and_compositions/subtitle.rst:88
msgid "**Spelling check**"
msgstr ""

#: ../../effects_and_compositions/subtitle.rst:90
msgid ""
"Spelling check for subtitle is integrated and shows incorrect words by a red "
"wiggly line. Right-click on the word and you get a list of possible words "
"you can choose by click on it."
msgstr ""
