# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-18 12:08+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../effects_and_compositions/effect_groups/distort/lens_correction.rst:13
msgid "Lens Correction"
msgstr "Lenscorrectie"

#: ../../effects_and_compositions/effect_groups/distort/lens_correction.rst:15
msgid "Contents"
msgstr "Inhoud"

#: ../../effects_and_compositions/effect_groups/distort/lens_correction.rst:17
msgid ""
"This is the `Frei0r lenscorrection <https://www.mltframework.org/plugins/"
"FilterFrei0r-lenscorrection/>`_ MLT filter."
msgstr ""
"Dit is het MLT-filter `Frei0r lenscorrectie <https://www.mltframework.org/"
"plugins/FilterFrei0r-lenscorrection/>`_."

#: ../../effects_and_compositions/effect_groups/distort/lens_correction.rst:19
msgid "Allows compensation of lens distortion."
msgstr "Biedt compensatie van lensvervorming"

#: ../../effects_and_compositions/effect_groups/distort/lens_correction.rst:21
msgid "https://youtu.be/axQdm482Uto"
msgstr "https://youtu.be/axQdm482Uto"

#: ../../effects_and_compositions/effect_groups/distort/lens_correction.rst:23
msgid "https://youtu.be/cEwZzNRiVks"
msgstr "https://youtu.be/cEwZzNRiVks"
