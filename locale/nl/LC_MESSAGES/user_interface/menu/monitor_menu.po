# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2021-12-31 15:58+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.0\n"

#: ../../user_interface/menu/monitor_menu.rst:16
msgid "Monitor Menu"
msgstr "Menu Monitor"

#: ../../user_interface/menu/monitor_menu.rst:21
msgid "Contents"
msgstr "Inhoud"

#: ../../user_interface/menu/monitor_menu.rst:27
msgid ""
"The monitor menu contains controls for viewing and navigating through the "
"clips in your project for the purpose of making edits and seeing the effects "
"of your changes.  Depending on which monitor window you have selected at the "
"time, the controls will affect either the currently selected clip in the "
"Project Bin (**Clip Monitor**) or the playhead in the Timeline (**Project "
"Monitor**)."
msgstr ""
"Het menu Monitor bevat besturing voor bekijken en navigeren door de clips in "
"uw project voor het doen van bewerkingen en bekijken van de effecten van uw "
"wijzigingen.  Afhankelijk van welk monitorvenster u hebt geselecteerd op dat "
"moment, zal de besturing ofwel de nu geselecteerde clip in de Project-bin "
"(**Clipmonitor**) beïnvloeden of de positie in de tijdlijn "
"(**Projectmonitor**)."

#: ../../user_interface/menu/monitor_menu.rst:30
msgid ""
"With the exception of the :menuselection:`Deinterlacer` and :menuselection:"
"`Interpolation` items, it is much more practical to perform the actions on "
"this menu using the associated keyboard shortcuts or the buttons at the "
"bottom of the monitor windows."
msgstr ""
"Met de uitzondering van de items :menuselection:`Deinterlacer` en :"
"menuselection:`Interpolatie`, is het veel praktischer om de acties uit "
"voeren op dit menu met de bijbehorende sneltoetsen of de knoppen onderaan de "
"monitorvensters."

#: ../../user_interface/menu/monitor_menu.rst:34
msgid "Play"
msgstr "Afspelen"

#: ../../user_interface/menu/monitor_menu.rst:39
msgid "Play Zone"
msgstr "Sectie afspelen"

#: ../../user_interface/menu/monitor_menu.rst:44
msgid "Loop Zone"
msgstr "Loop sectie"

#: ../../user_interface/menu/monitor_menu.rst:49
msgid "Loop selected clip"
msgstr "Loop geselecteerde clip"

#: ../../user_interface/menu/monitor_menu.rst:54
msgid "Go To"
msgstr "Ga naar"

#: ../../user_interface/menu/monitor_menu.rst:59
msgid "Rewind"
msgstr "Terugspoelen"

#: ../../user_interface/menu/monitor_menu.rst:64
msgid "Rewind 1 frame"
msgstr "1 frame terug"

#: ../../user_interface/menu/monitor_menu.rst:69
msgid "Rewind 1 second"
msgstr "1 seconde terug"

#: ../../user_interface/menu/monitor_menu.rst:74
msgid "Forward 1 Frame"
msgstr "1 frame vooruit"

#: ../../user_interface/menu/monitor_menu.rst:79
msgid "Forward 1 Second"
msgstr "1 seconde vooruit"

#: ../../user_interface/menu/monitor_menu.rst:84
msgid "Forward"
msgstr "Vooruit"

#: ../../user_interface/menu/monitor_menu.rst:89
msgid "Set Zone In"
msgstr "Zone-begin instellen"

#: ../../user_interface/menu/monitor_menu.rst:94
msgid "Set Zone Out"
msgstr "Zone-eind instellen"

#: ../../user_interface/menu/monitor_menu.rst:99
msgid "Switch monitor fullscreen"
msgstr "Fullscreen monitor"

#: ../../user_interface/menu/monitor_menu.rst:104
msgid "Deinterlacer"
msgstr "Deinterlacer"

#: ../../user_interface/menu/monitor_menu.rst:109
msgid "Interpolation"
msgstr "Interpolatie"

#: ../../user_interface/menu/monitor_menu.rst:114
msgid "Switch monitor"
msgstr "Verwissel monitor"

#: ../../user_interface/menu/monitor_menu.rst:119
msgid "Insert zone in project bin"
msgstr "Zone invoegen in project-bin"

#: ../../user_interface/menu/monitor_menu.rst:124
msgid "Insert zone in timeline"
msgstr "Zone invoegen in tijdlijn"
