# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Vit Pelcak <vpelcak@suse.cz>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-03-09 00:39+0000\n"
"PO-Revision-Date: 2022-06-27 16:47+0200\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 22.04.2\n"

#: ../../user_interface/monitors.rst:1
msgid "Clip monitor and project monitor in Kdenlive video editor"
msgstr ""

#: ../../user_interface/monitors.rst:1
msgid ""
"KDE, Kdenlive, clip, project, monitor, documentation, user manual, video "
"editor, open source, free, learn, easy"
msgstr ""

#: ../../user_interface/monitors.rst:22
msgid "Monitors"
msgstr "Monitory"

#: ../../user_interface/monitors.rst:24
msgid ""
"Kdenlive uses 2 monitor widgets to display your videos: Clip Monitor and "
"Project Monitor. A third monitor - the Record Monitor - previews video "
"capture. These monitors can be selected by clicking the corresponding tabs "
"which appear at the bottom of the monitor window."
msgstr ""

#: ../../user_interface/monitors.rst:27
msgid "Resizing the Monitors"
msgstr ""

#: ../../user_interface/monitors.rst:29
msgid ""
"You can resize the monitors by dragging the sizing widget. It is a bit "
"tricky to find the bottom widget. You need to hover just between the bottom "
"of the monitor tab and the timeline"
msgstr ""

#: ../../user_interface/monitors.rst:37
msgid "Monitor zoombar"
msgstr ""

#: ../../user_interface/monitors.rst:41
msgid ""
"The Monitors get zoom bars. To activate: hover over the timeline ruler and :"
"kbd:`CTRL + Mouse wheel`."
msgstr ""

#: ../../user_interface/monitors.rst:48
msgid ""
"Support for external monitor display using Blackmagic Design decklink cards."
msgstr ""

#: ../../user_interface/monitors.rst:53
msgid "Monitor toolbar"
msgstr ""

#: ../../user_interface/monitors.rst:60
msgid ""
"Support multiple guide overlays. Move with the mouse to the upper-right "
"corner of the monitor to access the toolbar."
msgstr ""

#: ../../user_interface/monitors.rst:65
msgid "Preview resolution"
msgstr ""

#: ../../user_interface/monitors.rst:72
msgid ""
"Preview resolution speeds up the editing experience by scaling the video "
"resolution of the monitors. It can be used of proxies instead."
msgstr ""

#: ../../user_interface/monitors.rst:77
msgid "Clip Monitor"
msgstr "Monitor klipu"

#: ../../user_interface/monitors.rst:79
msgid ""
"The Clip monitor displays the unedited clip that is currently selected in :"
"ref:`project_tree`."
msgstr ""

#: ../../user_interface/monitors.rst:85
msgid "Widgets on the Clip Monitor"
msgstr ""

#: ../../user_interface/monitors.rst:87
msgid ""
"**Insert Zone In Project Bin** button - click this to add the current zone "
"to the project bin. The selected zone will appear as child clip in the "
"project bin - like the clip shown as Zone1 in the screen shot."
msgstr ""

#: ../../user_interface/monitors.rst:89
msgid "**Set zone start** button - click this to set an 'in' point."
msgstr ""

#: ../../user_interface/monitors.rst:91
msgid "**Set zone end** button - click this to set an 'out' point."
msgstr ""

#: ../../user_interface/monitors.rst:93
msgid ""
"Zone duration indicator - selected by setting in and out points. Dragging "
"the clip from the clip monitor to the timeline when there is a selected zone "
"causes the selected zone, not the entire clip, to be copied to the timeline."
msgstr ""

#: ../../user_interface/monitors.rst:95
msgid ""
"Position Caret - can be dragged in the clip. (In ver >=0.9.4 and with OpenGL "
"turned on in :menuselection:`Settings --> Configure Kdenlive --> Playback`, "
"audio will play as you drag this.)"
msgstr ""

#: ../../user_interface/monitors.rst:97
msgid ""
"Timecode widget - type a timecode here and hit :kbd:`Enter` to go to an "
"exact location in the clip. Timecode is in the format *hours:minutes:seconds:"
"frames* (where frames will correspond to the number of frames per second in "
"your project profile)."
msgstr ""

#: ../../user_interface/monitors.rst:99
msgid ""
"Timecode arrows - can be used to change the current position of the clip in "
"the clip monitor."
msgstr ""

#: ../../user_interface/monitors.rst:102
msgid "Creating Zones in Clip Monitor"
msgstr ""

#: ../../user_interface/monitors.rst:104
msgid ""
"Zones are defined regions of clips that are indicated by a colored section "
"in the clip monitor's timeline - see item 3 above. The beginning of a zone "
"is set by clicking **[** (item 1 in the pic above). The end of a zone is set "
"by clicking **]** (item 2 in the pic above)"
msgstr ""

#: ../../user_interface/monitors.rst:107
msgid "Clip Monitor Right-click menu"
msgstr ""

#: ../../user_interface/monitors.rst:109
msgid ""
"The Clip Monitor has a right-click (context) menu as described :ref:`here "
"<clip_monitor_rightclick>`."
msgstr ""

#: ../../user_interface/monitors.rst:112
msgid "Seeking"
msgstr ""

#: ../../user_interface/monitors.rst:116
msgid ""
"Inside the clip monitor: hold down :kbd:`Shift` and move the mouse left/"
"right."
msgstr ""

#: ../../user_interface/monitors.rst:121
msgid "Drag audio or video only of a clip in timeline"
msgstr ""

#: ../../user_interface/monitors.rst:128
msgid ""
"Move with the mouse to the lower-left corner of the clip monitor to access "
"the Video/Audio icons. Hover with the mouse either over the audio or video "
"icon left click to drag either video or audio part into the timeline."
msgstr ""

#: ../../user_interface/monitors.rst:133
msgid "Project Monitor"
msgstr ""

#: ../../user_interface/monitors.rst:135
msgid ""
"The Project Monitor displays your project's timeline - i.e. the edited "
"version of your video."
msgstr ""

#: ../../user_interface/monitors.rst:141
msgid "Project Monitor Widgets"
msgstr ""

#: ../../user_interface/monitors.rst:143
msgid ""
"The position caret. Shows the current location in the project relative to "
"the whole project. You can click and drag this to move the position in the "
"project."
msgstr ""

#: ../../user_interface/monitors.rst:145
msgid ""
"The timecode widget. You can type a timecode here and press :kbd:`Enter` to "
"bring the Project Monitor to an exact location."
msgstr ""

#: ../../user_interface/monitors.rst:147
msgid ""
"Timecode widget control arrows. You can move the Project Monitor one frame "
"at a time with these."
msgstr ""

#: ../../user_interface/monitors.rst:150
msgid "Creating Zones in Project Monitor"
msgstr ""

#: ../../user_interface/monitors.rst:152
msgid ""
"You can use the **[** and **]** buttons to create a zone in the Project "
"Monitor the same way you make zones in the clip monitor. The zone will be "
"indicated by a colored bar both on the timeline and underneath the Project "
"Monitor."
msgstr ""

#: ../../user_interface/monitors.rst:157
msgid ""
"You can get Kdenlive to only render the selected zone - see :ref:"
"`remder_using_zone`."
msgstr ""

#: ../../user_interface/monitors.rst:160
msgid "Project Monitor Right-click menu"
msgstr ""

#: ../../user_interface/monitors.rst:162
msgid ""
"The project monitor has a right-click (context menu) as described :ref:`here "
"<project_monitor_rightclick>`."
msgstr ""

#: ../../user_interface/monitors.rst:167
msgid "Multicam Editing"
msgstr ""

#: ../../user_interface/monitors.rst:171
msgid ""
"Enable the multirack view via menu :menuselection:`Monitor --> Multitrack "
"view`."
msgstr ""

#: ../../user_interface/monitors.rst:176
msgid ""
"New multicam editing interface allows you to select a track in the timeline "
"by clicking on the project monitor."
msgstr ""

#: ../../user_interface/monitors.rst:181
msgid "Separate Clip and Project Monitors"
msgstr ""

#: ../../user_interface/monitors.rst:183
msgid ""
"You can click on the Tab names that label the Monitors and drag the monitor "
"out into its own window."
msgstr ""

#: ../../user_interface/monitors.rst:188
msgid ""
"To put the monitors back into the Tabbed view - click on the monitor's title "
"bar and drag the window on top of the other monitor window."
msgstr ""

#: ../../user_interface/monitors.rst:190
msgid ""
"If the monitor has no title bar (intermittent defect) then you can not do "
"this and you will need to reset kdenlive settings by deleting ~/.config/"
"kdenliverc"
msgstr ""
