# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Shinjo Park <kde@peremen.name>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-29 00:22+0000\n"
"PO-Revision-Date: 2022-05-08 01:48+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../effects_and_compositions/audio.rst:11
msgid "Audio"
msgstr "오디오"

#: ../../effects_and_compositions/audio.rst:13
msgid ""
"Kdenlive has some tools for handling audio. Beside the audio spectrum viewer "
"and some audio effects, you have following possibilities:"
msgstr ""

#: ../../effects_and_compositions/audio.rst:18
msgid "Audio Mixer"
msgstr "오디오 믹서"

#: ../../effects_and_compositions/audio.rst:25
msgid "The audio mixer has following function for each channel:"
msgstr ""

#: ../../effects_and_compositions/audio.rst:27
msgid "Channel number (audio track number) or Master channel"
msgstr "채널 번호(오디오 트랙 번호) 또는 마스터 채널"

#: ../../effects_and_compositions/audio.rst:28
msgid "Mute an audio channel"
msgstr "오디오 채널 음소거"

#: ../../effects_and_compositions/audio.rst:29
msgid "Solo an audio channel"
msgstr ""

#: ../../effects_and_compositions/audio.rst:30
msgid "Record audio direct on the track of the related audio channel"
msgstr ""

#: ../../effects_and_compositions/audio.rst:31
msgid "Opens the effect stack of the related audio channel"
msgstr ""

#: ../../effects_and_compositions/audio.rst:32
msgid "Balance the audio channel. Either with the slider or with values"
msgstr ""

#: ../../effects_and_compositions/audio.rst:33
msgid "Adjustment of the volume"
msgstr "음량 조정"

#: ../../effects_and_compositions/audio.rst:36
msgid "Multiple audio streams"
msgstr "다중 오디오 스트림"

#: ../../effects_and_compositions/audio.rst:40
msgid ""
"Multiple audio streams of a video clip. In clip properties on the tab audio "
"you can adjust and manipulate each audio stream. More details see here :ref:"
"`audio_properties`"
msgstr ""
