# Spanish translations for docs_kdenlive_org_effects_and_compositions___effect_groups___audio_correction___mono_amplifier.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2021.
# Eloy Cuadra <ecuadra@eloihr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_kdenlive_org_effects_and_compositions___effect_groups___audio_correction___mono_amplifier\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-29 00:22+0000\n"
"PO-Revision-Date: 2021-12-25 09:43+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.0\n"

#: ../../effects_and_compositions/effect_groups/audio_correction/mono_amplifier.rst:10
msgid "Mono Amplifier"
msgstr "Amplificador mono"

#: ../../effects_and_compositions/effect_groups/audio_correction/mono_amplifier.rst:12
msgid "Contents"
msgstr "Contenido"

#: ../../effects_and_compositions/effect_groups/audio_correction/mono_amplifier.rst:14
msgid ""
"This is the LADSPA filter number `1048 <https://www.mltframework.org/plugins/"
"FilterLadspa-1048/>`_ from MLT."
msgstr ""
"Este es el filtro LADSPA número `1048 <https://www.mltframework.org/plugins/"
"FilterLadspa-1048/>`_ de MLT."
