# Spanish translations for docs_kdenlive_org_effects_and_compositions___transitions___alphaxor.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2021.
# Eloy Cuadra <ecuadra@eloihr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_kdenlive_org_effects_and_compositions___transitions___alphaxor\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2021-11-14 04:22+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../effects_and_compositions/transitions/alphaxor.rst:11
msgid "alphaxor transition"
msgstr ""

#: ../../effects_and_compositions/transitions/alphaxor.rst:13
msgid "Contents"
msgstr "Contenido"

#: ../../effects_and_compositions/transitions/alphaxor.rst:15
msgid ""
"This is the `Frei0r alphaxor <https://www.mltframework.org/plugins/"
"TransitionFrei0r-alphaxor/>`_ MLT transition."
msgstr ""

#: ../../effects_and_compositions/transitions/alphaxor.rst:17
msgid "The alpha XOR operation."
msgstr ""

#: ../../effects_and_compositions/transitions/alphaxor.rst:19
msgid "Yellow clip has a triangle alpha shape with min=0 and max=618."
msgstr ""

#: ../../effects_and_compositions/transitions/alphaxor.rst:21
msgid "Green clip has rectangle alpha shape with min=0 and max=1000."
msgstr ""

#: ../../effects_and_compositions/transitions/alphaxor.rst:23
msgid "alphaxor is the transition in between."
msgstr ""
