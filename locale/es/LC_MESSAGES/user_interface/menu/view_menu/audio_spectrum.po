# Spanish translations for docs_kdenlive_org_user_interface___menu___view_menu___audio_spectrum.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2021.
# Eloy Cuadra <ecuadra@eloihr.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_kdenlive_org_user_interface___menu___view_menu___audio_spectrum\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-03-01 00:38+0000\n"
"PO-Revision-Date: 2022-03-28 12:13+0200\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../user_interface/menu/view_menu/audio_spectrum.rst:13
msgid "Audio Spectrum"
msgstr "Espectro de sonido"

#: ../../user_interface/menu/view_menu/audio_spectrum.rst:18
msgid "Contents"
msgstr "Contenido"

#: ../../user_interface/menu/view_menu/audio_spectrum.rst:20
msgid ""
"This allows you to monitor the audio properties of your clip in detail. The "
"graph only display data while the clip is playing in the clip or project "
"monitor."
msgstr ""

#: ../../user_interface/menu/view_menu/audio_spectrum.rst:23
msgid ""
"It graphs the loudness of the audio (in decibels - vertical axis) for each "
"audio frequency (horizontal axis) in the current frame. The blue curve is +- "
"the maximum over the previous few samples."
msgstr ""

#: ../../user_interface/menu/view_menu/audio_spectrum.rst:26
msgid ""
"See also :ref:`spectrogram` scope which displays a graphical representation "
"of the audio spectrum over the entire clip."
msgstr ""

#: ../../user_interface/menu/view_menu/audio_spectrum.rst:34
msgid ""
"For more information see :ref:`Granjow's blog "
"<audio_spectrum_and_spectrogram>` on Audio Spectrum"
msgstr ""
