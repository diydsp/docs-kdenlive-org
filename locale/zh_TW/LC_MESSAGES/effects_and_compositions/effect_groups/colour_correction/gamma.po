# Chinese translations for Kdenlive Manual package
# Kdenlive Manual 套件的正體中文翻譯.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Automatically generated, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-18 00:21+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../../effects_and_compositions/effect_groups/colour_correction/gamma.rst:14
msgid "Gamma"
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/gamma.rst:16
msgid "Contents"
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/gamma.rst:18
msgid ""
"This is the `Gamma <https://www.mltframework.org/plugins/FilterGamma/>`_ MLT "
"filter."
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/gamma.rst:20
msgid "Changes gamma color value."
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/gamma.rst:22
msgid "https://youtu.be/bceEqjbt2XM"
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/gamma.rst:24
msgid "https://youtu.be/v5KRzActKCQ"
msgstr ""
