# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-28 18:16+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/scale_and_tilt.rst:14
msgid "Crop, Scale and Tilt"
msgstr "Обрізання, масштабування і нахил"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/scale_and_tilt.rst:16
msgid "Contents"
msgstr "Зміст"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/scale_and_tilt.rst:18
msgid ""
"This effect was previously named as **Scale and Tilt** and **Crop, Scale and "
"Position**."
msgstr ""
"Цей ефект раніше мав назву **Масштабувати і нахилити** і **Обрізання, "
"масштаб і позиція**."

#: ../../effects_and_compositions/effect_groups/crop_and_transform/scale_and_tilt.rst:20
msgid ""
"This is the `Frei0r scale0tilt <https://www.mltframework.org/plugins/"
"FilterFrei0r-scale0tilt/>`_ MLT filter from Richard Spindler."
msgstr ""
"Це фільтр MLT `Frei0r scale0tilt <https://www.mltframework.org/plugins/"
"FilterFrei0r-scale0tilt/>`_ від Річарда Шпіндлера."

#: ../../effects_and_compositions/effect_groups/crop_and_transform/scale_and_tilt.rst:22
msgid "Scales, Tilts and Crops an Image"
msgstr "Масштабування, нахил та обрізання зображення"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/scale_and_tilt.rst:24
msgid "https://youtu.be/WV4bocj7ygw"
msgstr "https://youtu.be/WV4bocj7ygw"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/scale_and_tilt.rst:26
msgid ""
"See also :ref:`pan_and_zoom` which can do very similar things and may do "
"them better."
msgstr ""
"Див. також :ref:`pan_and_zoom`, який може виконувати дуже подібні "
"перетворення і може робити це краще."
