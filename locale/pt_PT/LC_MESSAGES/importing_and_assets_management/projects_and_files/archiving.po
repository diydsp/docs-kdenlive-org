# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-17 00:38+0000\n"
"PO-Revision-Date: 2022-05-20 18:00+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: menuselection ref projectmenu Kdenlive clips gz\n"
"X-POFile-SpellExtra: guilabel\n"

#: ../../importing_and_assets_management/projects_and_files/archiving.rst:1
msgid "The Kdenlive User Manual"
msgstr "O Manual de Utilizador do Kdenlive"

#: ../../importing_and_assets_management/projects_and_files/archiving.rst:1
msgid ""
"KDE, Kdenlive, documentation, user manual, video editor, open source, free, "
"help, archive, archiving"
msgstr ""
"KDE, Kdenlive, documentação, manual do utilizador, editor de vídeo, código "
"aberto, livre, ajuda, arquivo, arquivar"

#: ../../importing_and_assets_management/projects_and_files/archiving.rst:20
msgid "Archiving"
msgstr "Arquivo"

#: ../../importing_and_assets_management/projects_and_files/archiving.rst:26
msgid ""
"The Archiving feature (:menuselection:`Project --> Archive Project`, see :"
"ref:`project_menu`) in **Kdenlive** allows you to copy all files required by "
"the project (images, video clips, project files,...) to a folder, and "
"alternatively to compress the whole into a tar.gz or a zip file."
msgstr ""
"A funcionalidade de Arquivo (:menuselection:`Projecto --> Arquivar o "
"Projecto`, ver em :ref:`project_menu`) no **Kdenlive** permite-lhe copiar "
"todos os ficheiros necessários pelo projecto (imagens, 'clips' de vídeo, "
"ficheiros do projecto, ...) para uma pasta e, em alternativa, comprimir tudo "
"para um ficheiro .tar.gz ou .zip."

#: ../../importing_and_assets_management/projects_and_files/archiving.rst:28
msgid ""
"Archiving changes the project file to update the path of video clips to the "
"archived versions."
msgstr ""
"O arquivo muda o ficheiro do projecto para actualizar a localização dos "
"'clips' de vídeo para as versões arquivadas."

#: ../../importing_and_assets_management/projects_and_files/archiving.rst:30
msgid ""
"This can be useful if you finished working on a project and want to keep a "
"copy of it, or if you want to move a project from one computer to another."
msgstr ""
"Isto pode ser útil se acabar de trabalhar sobre um projecto e quiser manter "
"uma cópia do mesmo, ou se quiser mover um projecto de um computador para "
"outro."

#: ../../importing_and_assets_management/projects_and_files/archiving.rst:38
msgid ""
"The resulting tar.gz or zip file can be opened directly in **Kdenlive** "
"with :menuselection:`File --> Open`, then switch file ending to :guilabel:"
"`Archived Project`."
msgstr ""
"O ficheiro 'tar.gz' ou 'zip' resultante poderá ser aberto directamente no "
"**Kdenlive** com a opção :menuselection:`Ficheiro --> Abrir`, mudando depois "
"o fim do ficheiro para :guilabel:`Projecto Arquivado`."

#: ../../importing_and_assets_management/projects_and_files/archiving.rst:46
msgid ""
"Kdenlive will uncompress it to a location you specify before opening it."
msgstr ""
"O Kdenlive descomprimi-lo-á para um local indicado por si antes de o abrir."

#: ../../importing_and_assets_management/projects_and_files/archiving.rst:55
msgid ""
"If you have archived the project with the option :guilabel:`Archive only "
"timeline clips`, **Kdenlive** ask what it should do with the not archived "
"clips."
msgstr ""
"Se tiver arquivado o projecto com a opção :guilabel:`Arquivar apenas os "
"clips da linha temporal`, o **Kdenlive** pergunta o que deverá fazer com os "
"'clips' não arquivados."
