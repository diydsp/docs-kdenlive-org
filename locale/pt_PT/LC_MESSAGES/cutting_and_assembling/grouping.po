# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2022-05-22 13:25+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: clips Clips menuselection clip ref editing Clip\n"
"X-POFile-SpellExtra: clipmenu Kdenlive\n"

#: ../../cutting_and_assembling/grouping.rst:16
msgid "Grouping"
msgstr "Agrupamento"

#: ../../cutting_and_assembling/grouping.rst:21
msgid "Contents"
msgstr "Conteúdo"

#: ../../cutting_and_assembling/grouping.rst:23
msgid ""
"Grouping clips allows you to lock clips together so that you can move them "
"as a group and still retain their positions relative to each element in the "
"group."
msgstr ""
"Agrupar os 'clips' permite-lhe bloquear os mesmos em conjunto, para que os "
"possa mover como um grupo e mantendo à mesma as suas posições relativas a "
"cada elemento no grupo."

#: ../../cutting_and_assembling/grouping.rst:27
msgid "How to Group Clips"
msgstr "Como Agrupar os 'Clips'"

#: ../../cutting_and_assembling/grouping.rst:29
msgid ""
"You can select multiple clips in preparation for grouping them by holding "
"shift and clicking the mouse and dragging in the timeline."
msgstr ""
"Poderá seleccionar vários 'clips' em preparação para os agrupar, mantendo a "
"tecla Shift carregada e carregando no botão do rato e arrastando na linha "
"temporal."

#: ../../cutting_and_assembling/grouping.rst:37
msgid ""
"To group the selected clips select :menuselection:`Timeline --> Group Clips` "
"or right-click the selected clips and choose :menuselection:`Group Clips`."
msgstr ""
"Para agrupar os 'clips' seleccionados, seleccione a opção :menuselection:"
"`Linha Temporal --> Agrupar os Clips` ou carregue com o botão direito sobre "
"os 'clips' seleccionados e escolha a opção :menuselection:`Agrupar os Clips`."

#: ../../cutting_and_assembling/grouping.rst:42
msgid "Cutting Grouped Clips"
msgstr "Cortar os 'Clips' Agrupados"

#: ../../cutting_and_assembling/grouping.rst:44
msgid ""
"Grouping is also useful if you have separate audio and video tracks and need "
"to cut and splice both tracks at exactly the same point (e.g. for audio sync "
"reasons)."
msgstr ""
"O agrupamento também é útil se tiver faixas de vídeo e áudio separadas e "
"precise de as cortar e colar ambas exactamente no mesmo ponto (p.ex., por "
"razões de sincronização do áudio)."

#: ../../cutting_and_assembling/grouping.rst:47
msgid ""
"If you cut the video clip using the :ref:`editing` when there is an audio "
"clip grouped to it, then **Kdenlive** cuts the audio clip at the same point "
"automatically."
msgstr ""
"Se cortar o 'clip' de vídeo com a opção :ref:`editing` quando existir um "
"'clip' de áudio agrupado com ele, então o **Kdenlive** irá cortar o 'clip' "
"de áudio automaticamente no mesmo ponto."

#: ../../cutting_and_assembling/grouping.rst:65
msgid "Removing Clip Grouping"
msgstr "Remover o Agrupamento do 'Clip'"

#: ../../cutting_and_assembling/grouping.rst:67
msgid ""
"To remove the grouping on clips, select the group of clips and choose :"
"menuselection:`Timeline --> Ungroup Clips`."
msgstr ""
"Para remover o agrupamento dos 'clips', seleccione o grupo de 'clips' e "
"escolha  a opção :menuselection:`Linha Temporal --> Desagrupar os Clips`."

#: ../../cutting_and_assembling/grouping.rst:71
msgid "FAQ"
msgstr "FAQ"

#: ../../cutting_and_assembling/grouping.rst:73
msgid "Q: How to delete sound track only?"
msgstr "P: Como apagar apenas a faixa de som?"

#: ../../cutting_and_assembling/grouping.rst:75
msgid ""
"A: Right-click on the clip and choose :menuselection:`Split Audio`. The "
"audio will move to an audio track but be grouped with the video track."
msgstr ""
"R: Carregue com o botão direito sobre o 'clip' e escolha :menuselection:"
"`Dividir o Áudio`. O mesmo mover-se-á para uma faixa de áudio mas ficará "
"agrupado com a faixa de vídeo."

#: ../../cutting_and_assembling/grouping.rst:81
msgid "Right-click again and choose :menuselection:`Ungroup Clips`."
msgstr ""
"Carregue de novo no botão direito e escolha :menuselection:`Desagrupar os "
"Clips`."

#: ../../cutting_and_assembling/grouping.rst:83
msgid "Then you can delete just the audio track."
msgstr "Depois poderá então apagar apenas a faixa de áudio."

#: ../../cutting_and_assembling/grouping.rst:85
msgid ""
"Alternatively you can keep the audio in the clip and use the :menuselection:"
"`Audio Correction --> Mute` effect to just mute the soundtrack on the clip."
msgstr ""
"Em alternativa, poderá manter o áudio no 'clip' e usar o efeito :"
"menuselection:`Correcção do Áudio --> Silenciar` para simplesmente silenciar "
"a banda sonora do 'clip'."

#: ../../cutting_and_assembling/grouping.rst:87
msgid ""
"Yet another method is to select :menuselection:`Video only` from the :ref:"
"`clip_menu`."
msgstr ""
"Mais outro método será seleccionar :menuselection:`Apenas o vídeo` no :ref:"
"`clip_menu`."
